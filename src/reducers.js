import { combineReducers } from 'redux';
import colorReducer from './app/HexToRGB/duck';

const rootReducer = combineReducers({
  color: colorReducer,
});

export default rootReducer;
