import React from 'react';
import { shallow } from 'enzyme';
import '../../setupTest';

import { Footer } from './Footer';

let footer;

const props = {
  classes: {},
  isHomePage: true,
};

beforeEach(() => {
  footer = shallow(<Footer {...props} />);
});

describe('HexToRGB', () => {
  it('should show LinkedIn` logo', () => {
    expect(footer.find('img').length).toEqual(1);
  });
});
