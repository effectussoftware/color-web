import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import withStyles from 'react-jss';

import linkedinlogo from './linkedinlogo.png';
import linkedinWhite from './linkedInWhite.png';
import { HEXTORGB, COLORPICKER } from '../Utils/directions';
import styles from '../HexToRGB/styles';
import isDarkColor from '../Utils/isDarkColor';

export const Footer = (props) => {
  const { footer, linkedInLogo, link } = props.classes;
  return (
    <div className={footer}>
      <a href="https://www.linkedin.com/company/effectussoftware/">
        <img className={linkedInLogo} src={isDarkColor(props.color) ? linkedinWhite : linkedinlogo} alt="linkedinlogo" />
      </a>
      <Link to={props.isHomePage ? COLORPICKER : HEXTORGB} className={`${link} ${isDarkColor(props.color) && "white"}`}>
        {props.isHomePage ? 'Color Picker' : 'Hex to RGB converter'}
      </Link>
    </div>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  isHomePage: PropTypes.bool.isRequired,
};

export default withStyles(styles)(Footer);
