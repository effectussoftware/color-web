import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'react-jss';

import logo from './logo.png';
import logoWhite from './logo-white.png';
import styles from '../HexToRGB/styles';
import isDarkColor from '../Utils/isDarkColor';

export const Header = props => (
  <div className={props.classes.header}>
    <div className={props.classes.imageBack}>
      <a href="http://www.effectussoftware.com/">
        <img className={props.classes.image} src={isDarkColor(props.color) ? logoWhite : logo} alt="logo" />
      </a>
    </div>
  </div>
);

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.string,
};

export default withStyles(styles)(Header);
