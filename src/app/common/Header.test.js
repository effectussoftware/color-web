import React from 'react';
import { shallow } from 'enzyme';
import '../../setupTest';

import { Header } from './Header';

let header;

const props = {
  classes: {},
  color: '',
};

beforeEach(() => {
  header = shallow(<Header {...props} />);
});

describe('HexToRGB', () => {
  it('should show effects` logo', () => {
    expect(header.find('img').length).toEqual(1);
  });
});
