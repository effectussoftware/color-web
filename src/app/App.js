import React from 'react';
import { Route } from 'react-router-dom';
import { ThemeProvider } from 'react-jss';

import { HEXTORGB, COLORPICKER } from './Utils/directions';
import HexToRGBContainer from './HexToRGB/HexToRGBContainer';
import ColorPickerContainer from './ColorPicker/ColorPickerContainer';

const styles = {
  app: {
    height: '100%',
  },
};

const App = () => (
  <ThemeProvider theme={{}}>
    <div className="App" style={styles.app}>
      <Route path={HEXTORGB} exact component={HexToRGBContainer} />
      <Route path={COLORPICKER} exact component={ColorPickerContainer} />
    </div>
  </ThemeProvider>
);

export default App;
