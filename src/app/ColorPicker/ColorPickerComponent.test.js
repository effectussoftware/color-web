import React from 'react';
import { mount } from 'enzyme';
import '../../setupTest';

import { ColorPicker } from './ColorPickerComponent';

let colorPicker;

const props = {
  classes: {},
  changeColor: jest.fn(),
  color: '',
};

beforeEach(() => {
  colorPicker = mount(<ColorPicker {...props} />);
});

describe('ColorPicker', () => {
  it('should show an input', () => {
    expect(colorPicker.find('button').length).toBeGreaterThanOrEqual(1);
  });
});
