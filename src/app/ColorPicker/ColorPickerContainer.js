import React from 'react';
import withStyles from 'react-jss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import HexToRGB from './ColorPickerComponent';
import Header from '../common/Header';
import Footer from '../common/Footer';
import styles from '../HexToRGB/styles';

const ColorPickerContainer = ({ classes, color }) => (
  <div className={classes.bigContainer}>
    <Header color={color} />
    <HexToRGB />
    <Footer color={color} isHomePage={false} />
  </div>
);

ColorPickerContainer.propTypes = {
  color: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  color: state.color.color,
});

export default connect(mapStateToProps)(withStyles(styles)(ColorPickerContainer));
