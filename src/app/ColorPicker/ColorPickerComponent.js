import React, { Component } from 'react';
import withStyles from 'react-jss';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../HexToRGB/styles';
import { changeColor } from '../HexToRGB/duck/actions';
import isDarkColor from '../Utils/isDarkColor';

export class ColorPicker extends Component {
  constructor(props) {
    super(props);
    this.inputDiv = React.createRef();
    this.state = {
      isFirstTime: true,
    };
  }

  componentDidMount() {
    this.props.changeColor('39579A');
    this.inputDiv.current.focus();
  }

  getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  randomColor = () => {
    const color = this.getRandomColor();
    this.props.changeColor(color);
    this.setState({ isFirstTime: false });
    this.inputDiv.current.focus();
  };

  inputValue = () => {
    if (this.state.isFirstTime) {
      return 'Press Spacebar';
    }
    return `#${this.props.color}`;
  };

  render() {
    const { classes, color } = this.props;
    return (
      <button
        tabIndex="0"
        onClick={() => this.randomColor()}
        className={classes.colorPickerContainer}
        ref={this.inputDiv}
        type="button"
      >
        <div
          className={isDarkColor(color) ? classes.readOnlyInputWhite : classes.readOnlyInputBlack}
        >
          {this.inputValue()}
        </div>
      </button>
    );
  }
}

const { object, func, string } = PropTypes;

ColorPicker.propTypes = {
  classes: object.isRequired,
  changeColor: func.isRequired,
  color: string.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({ changeColor }, dispatch);

const mapStateToProps = state => ({
  color: state.color.color,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(ColorPicker));
