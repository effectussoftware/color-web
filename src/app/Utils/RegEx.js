export const isRgb = /rgb\((\d{1,3}),(\d{1,3}),(\d{1,3})\)/;
export const valueExtractor = /rgba?\(([^)]+)\)/;
export const splitter = / *, */;
