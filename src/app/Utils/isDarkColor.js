const isDarkColor = color => {
  const r = parseInt(color.substring(0, 2), 16);
  const g = parseInt(color.substring(2, 4), 16);
  const b = parseInt(color.substring(4, 6), 16);
  const e = 0.2126 * r + 0.7152 * g + 0.0722 * b;
  return e < 40;
}

export default isDarkColor;
