import React from 'react';
import withStyles from 'react-jss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import HexToRGB from './HexToRGBComponent';
import Header from '../common/Header';
import Footer from '../common/Footer';
import styles from './styles';

export function HexToRGBContainer(props) {
  const { classes, color } = props;
  return (
    <div className={classes.bigContainer}>
      <Header color={color} />
      <HexToRGB />
      <Footer color={color} isHomePage />
    </div>
  );
}

HexToRGBContainer.propTypes = {
  color: PropTypes.string.isRequired,
};

HexToRGBContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  color: state.color.color,
});

export default connect(mapStateToProps)(withStyles(styles)(HexToRGBContainer));
