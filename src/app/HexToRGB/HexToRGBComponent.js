import React, { Component } from 'react';
import withStyles from 'react-jss';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from './styles';
import { changeColor } from './duck/actions';
import { isRgb, valueExtractor, splitter } from '../Utils/RegEx';
import isDarkColor from '../Utils/isDarkColor';

export class HexToRGB extends Component {
  constructor(props) {
    super(props);

    this.state = {
      term: '',
      top: true,
      rgb: false,
      rgbValue: '',
    };
    this.upperInput = React.createRef();
  }

  componentDidMount() {
    this.props.changeColor('39579A');
    this.upperInput.current.focus();
  }

  convertHelper = (value, top) => {
    let newR = '';
    let newG = '';
    let newB = '';
    this.setState({
      top,
      term: value,
    });
    if (value.length === 6) {
      newR = parseInt(value.substring(0, 2), 16);
      newG = parseInt(value.substring(2, 4), 16);
      newB = parseInt(value.substring(4, 6), 16);
      this.validateColor(newR, newG, newB, value);
    } else if (value.length === 3) {
      newR = parseInt(value[0] + value[0], 16);
      newG = parseInt(value[1] + value[1], 16);
      newB = parseInt(value[2] + value[2], 16);
      this.validateColor(newR, newG, newB, value);
    } else if (value.match(isRgb)) {
      let newValue = value.match(valueExtractor)[1];
      newValue = newValue.split(splitter).map(Number);
      const t = newValue[0];
      const a = newValue[1];
      const s = newValue[2];
      this.validateRGB(t, a, s, value);
    } else {
      this.setState({ rgb: false });
      this.props.changeColor('39579A');
    }
  };

  validateRGB = (r, g, b, value) => {
    if (r < 256 && g < 256 && b < 256) {
      let color = '';
      if (r === 0) {
        color = color.concat('00');
      } else if (r < 10) {
        color = color.concat('0');
        color = color.concat(r.toString(16));
      } else {
        color = color.concat(r.toString(16));
      }
      if (g === 0) {
        color = color.concat('00');
      } else if (g < 10) {
        color = color.concat('0');
        color = color.concat(g.toString(16));
      } else {
        color = color.concat(g.toString(16));
      }
      if (b === 0) {
        color = color.concat('00');
      } else if (b < 10) {
        color = color.concat('0');
        color = color.concat(b.toString(16));
      } else {
        color = color.concat(b.toString(16));
      }
      this.props.changeColor(color);
      this.setState({ term: color, rgb: true, rgbValue: value });
    }
  };

  validateColor = (r, g, b, value) => {
    const nan = Number.isNaN(r) || Number.isNaN(g) || Number.isNaN(b);
    if (!nan) {
      if (value.length === 3) {
        const newValue = value[0] + value[0] + value[1] + value[1] + value[2] + value[2];
        this.props.changeColor(newValue);
        this.setState({ term: value });
      } else if (value.length === 6) {
        this.props.changeColor(value);
        this.setState({ term: value, rgb: false });
      }
    }
  };

  valueHelper = (isTop) => {
    const {
      top, term, rgb, rgbValue,
    } = this.state;
    if (top === isTop) {
      if (rgb) {
        return rgbValue;
      }
      return term;
    }
    return this.renderRGB();
  };

  renderRGB = () => {
    const { color } = this.props;
    if (this.state.rgb) {
      return `#${color}`;
    }
    if (color) {
      const r = parseInt(color.substring(0, 2), 16);
      const g = parseInt(color.substring(2, 4), 16);
      const b = parseInt(color.substring(4, 6), 16);
      const nan = Number.isNaN(r) || Number.isNaN(g) || Number.isNaN(b);
      const isDefaultColor = color === '39579A';
      if (
        (r === '' && g === '' && b === '')
        || nan
        || (isDefaultColor && this.state.term !== '39579A')
      ) {
        return '';
      }
      return `rgb(${r},${g},${b})`;
    }
    return '';
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <input
          className={isDarkColor(this.props.color) ? classes.inputWhite : classes.inputBlack}
          placeholder="hex"
          onChange={event => this.convertHelper(event.target.value, true)}
          value={this.valueHelper(true)}
          ref={this.upperInput}
        />
        <input
          className={isDarkColor(this.props.color) ? classes.inputWhite : classes.inputBlack}
          placeholder="rgb"
          value={this.valueHelper(false)}
          onChange={event => this.convertHelper(event.target.value, false)}
        />
      </div>
    );
  }
}

const { object, func, string } = PropTypes;

HexToRGB.propTypes = {
  classes: object.isRequired,
  changeColor: func.isRequired,
  color: string.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({ changeColor }, dispatch);

const mapStateToProps = state => ({
  color: state.color.color,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(HexToRGB));
