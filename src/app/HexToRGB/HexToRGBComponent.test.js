import React from 'react';
import { mount } from 'enzyme';
import '../../setupTest';

import { HexToRGB } from './HexToRGBComponent';

let hexToRGB;

const props = {
  classes: {},
  changeColor: jest.fn(),
  color: '',
};

beforeEach(() => {
  hexToRGB = mount(<HexToRGB {...props} />);
});

describe('HexToRGB', () => {
  it('should show an input', () => {
    expect(hexToRGB.find('div').length).toBeGreaterThanOrEqual(1);
  });

  describe('convertHelper', () => {
    describe('if we pass 123456', () => {
      it('should set state to term = 123456', () => {
        hexToRGB.instance().convertHelper('123456', true);

        expect(hexToRGB.state()).toEqual({
          term: '123456',
          top: true,
          rgb: false,
          rgbValue: '',
        });
      });
    });
    describe('if we pass 123', () => {
      it('should set state to term =  123', () => {
        hexToRGB.instance().convertHelper('123', true);

        expect(hexToRGB.state()).toEqual({
          term: '123',
          top: true,
          rgb: false,
          rgbValue: '',
        });
      });
    });
    describe('if we pass 1234', () => {
      it('should set state to trm = 1234', () => {
        hexToRGB.instance().convertHelper('1234', true);

        expect(hexToRGB.state()).toEqual({
          term: '1234',
          top: true,
          rgb: false,
          rgbValue: '',
        });
      });
    });
    describe('if we pass rgb(43,43,43)', () => {
      it('should set state to trm = 2b2b2b', () => {
        hexToRGB.instance().convertHelper('rgb(43,43,43)', true);

        expect(hexToRGB.state()).toEqual({
          term: '2b2b2b',
          top: true,
          rgb: true,
          rgbValue: 'rgb(43,43,43)',
        });
      });
    });
  });
});
