import colorReducer from './colorReducer';
export { CHANGE_COLOR } from './types';
export default colorReducer;
