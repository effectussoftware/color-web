import { CHANGE_COLOR } from './types';

const INITIAL_STATE = {
  color: '39579A',
};

export default function colorChange(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHANGE_COLOR:
      return { ...state, color: action.payload };
    default:
      return state;
  }
}
