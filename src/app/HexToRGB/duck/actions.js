import { CHANGE_COLOR } from './types';

export function changeColor(value) {
  return {
    type: CHANGE_COLOR,
    payload: value,
  };
}
