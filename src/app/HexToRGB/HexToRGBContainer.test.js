import React from 'react';
import { shallow } from 'enzyme';

import { HexToRGBContainer } from './HexToRGBContainer';
import '../../setupTest';
import HexToRGB from './HexToRGBComponent';
import Header from '../common/Header';
import Footer from '../common/Footer';

let hexToRGBContainer;

const props = {
  classes: {},
  color: '',
};

beforeEach(() => {
  hexToRGBContainer = shallow(<HexToRGBContainer {...props} />);
});

describe('HexToRGB', () => {
  it('should show the HexToRGB component', () => {
    expect(hexToRGBContainer.find(HexToRGB).length).toBeGreaterThanOrEqual(1);
  });
  it('should show the a Header', () => {
    expect(hexToRGBContainer.find(Header).length).toBeGreaterThanOrEqual(1);
  });
  it('should show the a Footer', () => {
    expect(hexToRGBContainer.find(Footer).length).toBeGreaterThanOrEqual(1);
  });
});
